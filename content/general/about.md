---
title: "about"
date: 2020-06-22T21:31:30+02:00
draft: true
---

# Konrad's Page

Welcome to my website! I am currently working on my Master's degree in Computer Science, and am using this website to document some of my academic project efforts. Currently, the website only offers information on my programs for game AIs, but I might add more content in the future. 

Thanks for stopping by!