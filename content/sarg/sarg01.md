---
title: "Sarg Introduction"
date: 2020-06-22T15:52:57+02:00
draft: false
tags: ["sarg", "introduction"]
categories: ["sarg"]
---

# Sarg Introduction
## Documentation outline
Konrad Ukens, s0572411

HTW BERLIN IMI Master, Sommersemester 2020

AI for Games and interactive Systems

***

In the following I am going to document the process of developing my implementation of a game AI for the SARG game for the WP course 'Artificial Intelligence for Games and interactive Systems'. 
SARG is a server-run, round based board game similar to checkers, but is played by three players on a hexagonal board with hexagonal playing fields. A screenshot of the gameboard can be seen below: 

{{< figure src = "../sarg_spielbrett.PNG" title = "SARG Game Board" >}}

When it is a players turn, they choose a single stone to move with, which then 'jumps' to the two forward diagonal fields (e.g. the red stone at 0,0 will create two new stones at 0,1 and 1,1), and disappears from its originating position. If there are stones in its way, it will jump over these positions until the next free position (or the end of the playing board), making all the stones (so both those of its own color as well as those of either enemy) on it's path disappear. Whenever a stone jumps over the edge of the board, the playing player gets a point. Whoever reaches 5 points first, wins. 

